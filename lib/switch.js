function switchModule(mod, conf)
{
	var val = false;
	
	function debug(msg, level)
	{
		if(conf.debug >= level)
			console.log(msg);
	}
	
	function get()
	{
		return val;
	}
	
	function set(v)
	{
		val = v;
		mod.write(conf.pin, !v);
	}
	
	function main()
	{
		mod.setup(conf.pin, 'w', true);
	}
	main();
	
	this.get = get;
	this.set = set;

	return this;
}

module.exports = switchModule;