var Util, Exec, Wol, Http;

function netModule(conf, events)
{
	var list = [];
	var timeouti;
	
	function set(value)
	{
		if(typeof value != 'number' || !(value >= 0))
			value = -1;
		if(events.set instanceof Function)
			events.set(value);
	}
	
	function modSet(value)
	{
		if(typeof value != 'number' || !(value >= 0))
			return;
		
		clearTimeout(timeouti);
		timeouti = setTimeout(set, 300);
		
		var cmd = list[value];
		
		if(cmd instanceof Object)
		switch(cmd.type)
		{
			case 'exec':
				
				if(Exec == null)
					Exec = child_process.exec;
				Exec(cmd.exec, function(e,so,se) {});
				
				break;
			
			case 'wol':
				
				if(Wol == null)
					Wol = require('wake_on_lan');
				Wol.wake(cmd.mac);
				
				break;
			
			case 'curl':
				
				if(Http == null)
					Http = require('http');
				var req = Http.request(cmd.request, function() {});
				req.on('error', function(e) {});
				if(cmd.data != null)
					req.write(cmd.data);
				req.end();
				
				break;
		}
	}
	
	function init()
	{
		if(!(conf instanceof Object))
			conf = {};
		if(conf.list instanceof Array)
			list = conf.list;
	}
	
	init();
	
	this.set = modSet;
	return this;
}

module.exports = netModule;