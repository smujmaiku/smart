/*

 * @name JavaScript/NodeJS Socket
 * @author Michael Szmadzinski
 * @contact smujco@gmail.com

 * Copyright 2015 Michael Szmadzinski - MIT license

 * npm install express express-session cookie-parser
 * npm install socket.io socket.io-client
 * http://socket.io/docs/server-api/
 * http://socket.io/docs/client-api/

*/

var Express = require('express');
var CookieParser = require('cookie-parser');
var Session = require('express-session');
var fs = require('fs');
var Url = require('url');

var Http = require('http');
var Https = require('https');
var Io = null;	//require('socket.io');
var IoClient = null;	//require('socket.io-client')

var debugmode = false;

function cleanData(dataorg, keys)
{
	var data = '';
	try
	{
		data = JSON.parse(JSON.stringify(dataorg));
	} catch(e) { return data; }

	if(data instanceof Object)
	{
		if(!(keys instanceof Array))
			keys = ['password', 'pass', 'secret'];

		for(i in keys)
		{
			if(data[keys[i]] != null)
				data[keys[i]] = '*';
			if(data[keys[i]] instanceof Object || data[keys[i]] instanceof Array)
				data[keys[i]] = cleanData(data[keys[i]]);
		}
	}

	if(data instanceof Array)
		for(i in data)
			data[i] = cleanData(data[i]);

	return data;
}
	
function debug(data)
{
	if(!debugmode)
		return;
	console.log(cleanData(data));
}

function serverModule(port, httpdir, apiList, sockList, option)
{
	var app;
	var http;
	var io;
	
	var sockAgents = {};

	function sockListAgents()
	{
		return Object.keys(sockAgents);
	}
	
	function sockSend(id, evt, data)
	{
		if(id == null)
		{
			id = new Array();
			for(i in sockAgents)
				id.push(i);
		}
		
		if(id instanceof Array)
		{
			var success = true;
			for(var i = 0; i < id.length; i++)
				if(!sockSend(id[i], evt, data))
					success = false;
			return success;
		}
		
		if(typeof id != 'string')
			return false;
		
		var sock = sockAgents[id];
		if(sock == null)
			return false;
		
		sock.emit(evt, data);
		debug('Send( ' + id.substr(id.length - 4) + ' : ' + evt + ' ): ' + JSON.stringify(cleanData(data)));
		
		return true;
	}
	
	function sockSendBatch(batch)
	{
		var success = true;
		for(id in batch)
		{
			var list = batch[id];
			for(evt in list)
			{
				var data = list[evt];
				if(!sockSend(id, evt, data))
					success = false;
			}
		}
		
		return success;
	}
	
	function apiRequest(req, res, next)
	{
		var id = req.session.id;
		var url = req._parsedUrl.pathname;
		var ret = apiList[url];
		
		debug('apiRequest: ' + url);
	
		if(ret == null)
		{
			res.writeHead(500);
			res.end('Error loading API: ' + url);
			return;
		}
	
		var data = ret(id, req.query);
		res.writeHead(200, {'Content-Type': 'text/plain'});
		if(typeof data == "string")
			res.end(data);
		else
			res.end(JSON.stringify(data));
	}
	
	function socketLoadEvent(id, socket, evt, ret)
	{
		debug('LoadEvent( ' + id.substr(id.length - 4) + ' ): ' + evt);
		
		//if(ret == null)
		//	ret = sockList[evt];

		socket.on(evt, function(data)
		{
			if(ret != null)
			{
				debug('Event( ' + id.substr(id.length - 4) + ' : ' + evt + ' ): ' + JSON.stringify(cleanData(data)));

				var retdata = ret(id, data);
				if(retdata != null)
					sockSend(id, 'return', retdata);
			}
			else
				debug('Event( ' + id.substr(id.length - 4) + ' : ' + evt + ' ): Missing sock function');
		});
	}
	
	function addEvent(id, events)
	{
		if(id instanceof Array)
		{
			for(var i = 0; i < id.length; i++)
				addEvent(id[i], events);
			return;
		}
		
		var socket = sockAgents[id];
		if(socket == null)
			return false;
		
		for(var key in events)
			socketLoadEvent(id, socket, key, events[key]);
	}
	
	function removeEvent(id, events)
	{
		if(id instanceof Array)
		{
			for(var i = 0; i < id.length; i++)
				removeEvent(id[i], events);
			return;
		}
		
		var socket = sockAgents[id];
		if(!(events instanceof Array))
			events = [events];
		
		for(var i = 0; i < events.length; i++)
			socket.removeListener(events[i], function(){});
	}
	
	function socketConnect(socket)
	{
		var id = socket.id;
		sockAgents[id] = socket;
		
		debug('Connect: ' + id);
		
		for(key in sockList)
		{
			if(['connect', 'disconnect'].indexOf(key) < 1)
				socketLoadEvent(id, socket, key, sockList[key]);
		}
	
		//if(sockList['disconnect'] == null)
		socket.on('disconnect', function(data)
		{
			if(sockList['disconnect'] != null)
				sockList['disconnect'](id, socket);
			delete sockAgents[id];
			debug('Disconnect: ' + id);
		});
		
		if(sockList['connect'] != null)
		{
			var retdata = sockList['connect'](id, socket);
			if(retdata != null)
				sockSend(id, 'return', retdata);
		}
	}
	
	function init()
	{
		if(typeof option == 'boolean')
			option = {'debug': option};
		if(!(option instanceof Object))
			option = {};
		
		if(option['debug'] == true)
			debugmode = true;
		if(option['sessionSecret'] == null)
			option['sessionSecret'] = 'OP5CaZ8STJL1S';
			
		app = Express();
		var ssl = option['sslKey'] != null && option['sslCert'] != null;
		if(ssl)
			http = Https.createServer({key: fs.readFileSync(option['sslKey']), cert: fs.readFileSync(option['sslCert']), ca: (option['sslCa']==null? '': fs.readFileSync(option['sslCa']))}, app);
		else
			http = Http.createServer(app);

		if(sockList instanceof Object)
		{
			if(Io == null)
				Io = require('socket.io');

				//Websocket socket io
			io = Io(http);
			io.sockets.on('connection', socketConnect);
		}
		
		if(apiList instanceof Object)
		{
				//Do API calls
			app.use(CookieParser());
			app.use(Session({secret: option['sessionSecret'], 'resave': true, 'saveUninitialized': true}));
			for(key in apiList)
				app.get(key, apiRequest);
		}
	
			//Static Catch all
		app.get('*', Express.static(httpdir));
		
		http.listen(port);
		console.log('Listing on port ' + port + ' with http' + ((ssl) ? 's' : '') + ((apiList instanceof Object) ? ', api' : '') + ((sockList instanceof Object) ? ', io' : ''));
	}
	
	init();
	
	this.sockListAgents = sockListAgents;
	this.sockSend = sockSend;
	this.sockSendBatch = sockSendBatch;
	this.addEvent = addEvent;
	this.removeEvent = removeEvent;

	return this;
}

function clientModule(url, sockList, options, id)
{
	var socket;

	function send(evt, data)
	{
		socket.emit(evt, data);
		debug('Send( Client-' + id + ' : ' + evt + ' ): ' + JSON.stringify(cleanData(data)));
	}

	function socketLoadEvent(evt)
	{
		debug('LoadEvent( Client-' + id + ' ): ' + evt);

		var ret = sockList[evt];
		socket.on(evt, function(data)
		{
			if(ret != null)
			{
				debug('Event( Client-' + id + ' : ' + evt + ' ): ' + JSON.stringify(cleanData(data)));

				var retdata = ret(data, id);
				if(retdata != null)
					sockSend(id, 'return', retdata);
			}
			else
				debug('Event( Client-' + id + ' : ' + evt + ' ): Missing sock function');
		});
	}
	
	function init()
	{
		if(IoClient == null)
			IoClient = require('socket.io-client');

		socket = new IoClient(url);
		
		for(key in sockList)
			socketLoadEvent(key);
		
		if(sockList['disconnect'] == null)
			socket.on('disconnect', function(data) { debug('Disconnect( Client-' + id + ' : ' + evt + ' ): ' + JSON.stringify(cleanData(data))); } );
		if(sockList['return'] == null)
			socket.on('return', function(data) { debug('Return( Client-' + id + ' : ' + evt + ' ): ' + JSON.stringify(cleanData(data))); } );
	}

	init();

	this.send = send;

	return this;
}

function sendCurl(url, copts, rfunc, id)
{
	var optslist = {};
	
	if(typeof url !== 'string')
		return false;
	if(!(copts instanceof Object))
		copts = {copts: copts};

	options = Url.parse(url);
	options['headers'] = {};
	options['headers']['cookie'] = copts['cookie'];
	options['method'] = copts['method'];
	
	if(copts['user'] != null)
		options['auth'] = copts['user'] + ((copts['pass'] == null) ? '' : ':' + copts['pass']);

	if(typeof copts['data'] == 'string')
		options['headers']['Content-Length'] = copts['data'].length;

	var protocol = Http;
	if(options['protocol'].toLowerCase() == 'https:')
	{
		protocol = Https;
		if(copts.insecure == true)
			options.rejectUnauthorized = false;
	}
		
	
	for(var i in optslist)
		if(Object.keys(copts).indexOf(i) > -1)
			options[optslist[i]] = copts[i];

	var req = protocol.request(options, function(res)
	{
		if(res.statusCode == 200 || !copts['onlyOk'])
		{
			res.setEncoding('utf8');
			if(copts['nodata'])
				rfunc(null, res.statusCode, res.headers, id);
			var data = '';
			res.on('data', function(chunk)
			{
				data += chunk;
			});
			res.on('end', function()
			{
				if(rfunc instanceof Function && !copts['nodata'])
					rfunc(data, res.statusCode, res.headers, id);
			});
		}
	});

	if(copts['timeout'] > 0)
		req.setTimeout(copts['timeout'], function() {
			debug('Curl(timeout): ' + url);
		});

	req.on('error', function(e) {
		debug('Curl(' + options['hostname'] + '): ' + e.message);
	});

	if(copts['data'] != null)
	{
		var data = copts['data'];
		if(data instanceof Object)
			data = querystring.stringify(data);
		if(typeof data == 'string')
			req.write(data);
	}

	req.end();
}

module.exports = serverModule;
module.exports.createServer = serverModule;
module.exports.createClient = clientModule;
module.exports.sendCurl = sendCurl;

module.exports.debugmode = debugmode;

/* html example

<script language="javascript" src="http://smujbar.com:8033/socket.io/socket.io.js"></script>
<script language="javascript">
var socket;
function socketSend(evt, data)
{
	socket.emit(evt, data);
}
function socketConnect(url, sockList)
{
	socket = io.connect(url);
	for(key in sockList)
		socket.on(key, sockList[key]);
	if(sockList['disconnect'] == null)
		socket.on('disconnect', function(data) { console.log(data); } );
	if(sockList['return'] == null)
		socket.on('return', function(data) { console.log(data); } );
}
</script>

//*/


