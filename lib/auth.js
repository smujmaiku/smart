/*

 * @name JavaScript/NodeJS auth.js
 * @author Michael Szmadzinski
 * @contact mszmadzinski@liquidweb.com

 * This module is designed to store ldap information and authenticate
 * conf(igurations) are described in the README.md

*/

	//Modules
var fs = require('fs');
var Ldap;
var md5 = require('md5');

function authModule(conf, events)
{
		//Modules
	var ldapclient;

		//Globals
	var authlist = {};
	var ldapusers = {};
	var ldapauthlist;
	var sesslist = {};
	var authcooldown = {};
	
	/*
	 * Log to screen per debug level
	 */
	function debug(msg, level)
	{
		if(conf.debug >= level)
			console.log(msg);
	}
	
	/*
	 * Create new session
	 */
	function newSess(user, group)
	{
		var sessid;
		
			//Store new session
		if(typeof user == 'string')
		{
			sessid = Math.random().toString(36).substring(2)
				+ '.' + Math.random().toString(36).substring(2).toUpperCase()
				+ '_' + new Date().getTime().toString(36);
			
			sesslist[sessid] = {
				user: user,
				group: group,
				expire: new Date().getTime() + 24*60*60*1000
			};
		}
		
			//Remove expired
		for(var key in sesslist)
			if(sesslist[key].expire < new Date().getTime())
				delete sesslist[key];
		
			//Write file
		fs.writeFile('./var/auth.sess.json', JSON.stringify(sesslist, null, "\t"));
		
		return sessid;
	}
	
	/*
	 * Return ldap users Array
	 */
	function ldapList()
	{
		return Object.keys(ldapusers);
	}
	
	/*
	 * Return ldap user Object
	 *
	 * Param{String}: userid
	 */
	function ldapGet(userid)
	{
		return ldapusers[userid];
	}
	
	/*
	 * Connect to ldap
	 */
	function ldapConnect()
	{
		ldapGetlist();
		ldapClose();
		
			//Check for ldapurl
		if(typeof conf.ldapurl != 'string')
			return;
		
			//Load ldap module
		if(!(Ldap instanceof Object))
			Ldap = require('LDAP');
		
			//Create client
		ldapclient = new Ldap({
			uri: conf.ldapurl,
			starttls: conf.ldapstarttls,
			connecttimeout: 1,
			reconnect: true,
			timeout: 10000
		});
		
		var tries = 0;
		
			//ldap open event
		function onOpen(e)
		{
			if(e != null)
			{
				debug('auth(open): ' + (new Date()).toJSON(), 1);
				return debug(e, 1);
			}
			
				//Adjust bind
			var bind = conf.ldapbind.replace(/{user}/, conf.ldapuser);
			
				//Bind connection
			var out = ldapclient.simplebind({
				binddn: bind,
				password: conf.ldappass
			}, onBind);
			
			debug('bind: ' + out, 2);
		}
		
			//ldap bind event
		function onBind(e)
		{
			if(e != null)
			{
				debug('auth(bind): ' + (new Date()).toJSON(), 1);
				return debug(e, 1);
			}
			
				//Search ldap
			var out = ldapclient.search({
				base: conf.ldapbase,
				scope: 'sub',
				filter: conf.ldapfilter
			}, onSearch);
			
			debug('search: ' + out, 2);
		}
		
			//ldap search event
		function onSearch(e, data)
		{
			if(e != null)
			{
				tries++;
				if(tries < 10);
					setTimeout(onBind, 3000);
				debug('auth(search): ' + (new Date()).toJSON(), 1);
				return debug(e, 1);
			}
			
				//Store users
			if(data instanceof Array)
			while(data.length > 0)
			{
				var user = data.pop();
				ldapusers[user.uid] = user;
			}
			
				//Update users
			if(events.users instanceof Function)
				events.users(ldapList());
		}
		
			//Open Connection
		var out = ldapclient.open(onOpen);
		
		debug('open: ' + out, 2);
	}
	
	/*
	 * Close ldap
	 */
	function ldapClose()
	{
		try
		{
			ldapclient.close();
		} catch(e) {}
	}
	
	/*
	 * Match Data
	 *
	 * @Param{String}: match
	 * @Param{Object}: data
	 */
	function matchData(match, data)
	{
			//Check for string match
		if(typeof data == 'string')
			return data.search(new RegExp('^(' + match + ')$', 'i')) >= 0;
		
			//Loop any arrays
		if(data instanceof Array)
		{
			if(data.length < 1)
				return false;
			
				//Recursively check Array
			for(var i = 0; i < data.length; i++)
				if(matchData(match, data[i]))
					return true;
			
			return false;
		}
		
			//Fail
		return false;
	}
	
	/*
	 * ldap Group
	 *
	 * @Param{String}: user
	 */
	function ldapGroup(user)
	{
			//Fail if we aren't configured
		if(!(ldapauthlist instanceof Object))
			return null;
		
			//Fail if we haven't a user
		var data = ldapusers[user];
		if(!(data instanceof Object))
			return null;
		
			//Loop options
		for(var group in ldapauthlist)
		{
			var authchecks = ldapauthlist[group];
			
			var pass = -1;
			if(authchecks instanceof Object)
			for(var key in authchecks)
			{
					//Extra checks for emtpy/failed objects
				if(pass == -1)
					pass = true;
				
					//Store match string
				var match = authchecks[key];
				
					//Store data
				var udata = data[key];
				
					//Check if failed
				if(matchData(match, udata) !== true)
				{
					pass = false;
					break;
				}
			}
			
				//Return success
			if(pass === true)
				return group;
		}
		
		return null;
	}
	
	
	/*
	 * ldap Check Authentication
	 *
	 * @Param{String}: user
	 * @Param{String}: pass
	 * @Param{Function}: callback
	 */
	function ldapCheckauth(user, pass, callback)
	{
			//Check if we have ldap data
		if(!(ldapusers instanceof Object))
			return false;
		if(Object.keys(ldapusers).length < 1)
			return false;
		
			//Check if we are even ldaping
		if(!(callback instanceof Function))
			return false;
		if(typeof conf.ldapurl != 'string')
			return false;
		if(!(Ldap instanceof Object))
			return false;
		
			//Verify credential data isn't awful
		if(typeof user != 'string')
			return false;
		if(user.length < 1)
			return false;
		if(typeof pass != 'string')
			return false;
		if(pass.length < 1)
			return false;
		
		debug('auth(ldapauth): ' + user, 2);
		
			//Create client
		var authclient = new Ldap({
			uri: conf.ldapurl,
			starttls: conf.ldapstarttls,
			connecttimeout: 1,
			reconnect: false,
			timeout: 5000
		});
		
			//ldap open event
		function onOpen(e)
		{
			if(e != null)
				return callback();
			
				//Adjust bind
			var bind = conf.ldapbind.replace(/{user}/, user);
			
				//Bind
			authclient.simplebind({
				binddn: bind,
				password: pass
			}, onBind);
		}
		
			//ldap bind event
		function onBind(e)
		{
			if(e != null)
				return callback();
			
				//Store group
			var group = ldapGroup(user);
			
				//Return callback
			callback(user, group, newSess(user, group));
		}
		
			//Open connection
		authclient.open(onOpen);
		
		return true;
	}
	
	/*
	 * Check user auth
	 *
	 * @Param{String}: user
	 * @Param{String}: pass
	 * @Param{Function}: callback
	 */
	function checkAuth(user, pass, callback)
	{
		if(!(callback instanceof Function))
			return false;
		
			//Process sessions
		if(user == null)
		{
				//Store session data
			var data = sesslist[pass];
			
			process.nextTick(function()
			{
					//Callback event
				if(data instanceof Object)
					callback(data.user, data.group, newSess(data.user, data.group))
				else
					callback();
				
					//Remove old session
				delete sesslist[pass];
				newSess();
			});
			
			return true;
		}
		
			//Check for brutes
		if(authcooldown[user] > (new Date()).getTime())
			return process.nextTick(callback);
		authcooldown[user] = (new Date()).getTime() + conf.authtimeout * 1000;
		
			//Process manual auth
		if(authlist instanceof Object)
		{
				//Store auth data
			var data = authlist[user];
			
				//Check valid user
			if(data instanceof Object)
			{
				process.nextTick(function()
				{
						//Callback event
					if(data.pass == md5(pass))
						callback(user, data.group, newSess(user, data.group));
					else
						callback();
				});
				
				return true;
			}
		}
		
			//Process ldap auth
		if(ldapCheckauth(user, pass, callback) === true)
			return true;
		
			//Callback event fail
		process.nextTick(callback);
		
		return true;
	}
	
	/*
	 * Get auth users list
	 */
	function ldapGetlist()
	{
			//Load auth.ldap overrides
		if(!(ldapauthlist instanceof Object))
			try
			{
				ldapSetlist(fs.readFileSync('./var/auth.ldap.json', 'utf8'));
			} catch(e) {}
		
			//Load auth.ldap conf
		if(!(ldapauthlist instanceof Object))
			try
			{
				if(typeof conf.ldaplistfile == 'string')
					ldapSetlist(fs.readFileSync(conf.ldaplistfile, 'utf8'));
			} catch(e) {}
		
			//Store empty auth.ldap
		if(!(ldapauthlist instanceof Object))
			ldapauthlist = {};
		
		return ldapauthlist;
	}
	
	/*
	 * Set auth users list
	 *
	 * @Param{Object}: data
	 */
	function ldapSetlist(data, dowrite)
	{
			//Convert string to Object
		if(typeof data == 'string')
			try
			{
				data = JSON.parse(data);
			} catch(e) { return; }
		
			//Verify Object
		if(!(data instanceof Object))
			return;
		
			//Store data
		ldapauthlist = data;
		
			//Write to list file
		if(dowrite)
			try
			{
				fs.writeFileSync('./var/auth.ldap.json', JSON.stringify(ldapauthlist, null, '\t'));
			} catch(e) {}
	}
	
	/*
	 * Clear session list
	 */
	function sessClear()
	{
		sesslist = {};
	}
	
	/*
	 * Init
	 */
	function init()
	{
		if(!(conf instanceof Object))
			conf = {};
		if(!(events instanceof Object))
			events = {events: events};
		
			//Try ldap connect
		ldapConnect();
		
			//Load manual auth
		if(typeof conf.authfile == 'string')
			try
			{
				authlist = JSON.parse(fs.readFileSync(conf.authfile, 'utf8'));
			} catch(e) {}
		
			//Load sess auth
		try
		{
			sesslist = JSON.parse(fs.readFileSync('./var/auth.sess.json', 'utf8'));
		} catch(e) {}
	}
	
	init();
	
	this.ldapList = ldapList;
	this.ldapGet = ldapGet;
	this.ldapClose = ldapClose;
	this.ldapCheckauth = ldapCheckauth;
	this.ldapGetlist = ldapGetlist;
	this.ldapSetlist = ldapSetlist;
	this.sessClear = sessClear;
	this.checkAuth = checkAuth;

	return this;
}

module.exports = authModule;