function ledModule(conf, events)
{
	var list = [];
	var board;
	var pins = [];

	function hue2rgb(p, q, t)
	{
		if(t < 0) t += 1;
		if(t > 1) t -= 1;
		if(t < 1 / 6) return p + (q - p) * 6 * t;
		if(t < 1 / 2) return q;
		if(t < 2 / 3) return p + (q - p) * (2/3 - t) * 6;
		return p;
	}
	
	function hslToRgb(h, s, l)
	{
		var r, g, b;
		
		if(s <= 0)
			r = g = b = l;
		else
		{
			var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
			var p = 2 * l - q;
			r = hue2rgb(p, q, h + 1/3);
			g = hue2rgb(p, q, h);
			b = hue2rgb(p, q, h - 1/3);
		}
		return [Math.round(r * 255), Math.round(g * 255), Math.round(b * 255)];
	}
	
	function _color(hue, lum)
	{
		var sat = 1;
		if(isNaN(hue))
			sat = 0;
		return hslToRgb((hue/16)%1, sat, lum);
	}
	
	function set(value)
	{
		if(events.set instanceof Function)
			events.set(value);
	}
	
	function modSet(value)
	{
		switch(conf.type)
		{
			case 'switch':
				
				if(typeof value == 'boolean')
					pins[0].stop()[value ? 'on' : 'off']();
				
				break;
			
			case 'range':
				
				
				
				break;
			
			case 'color':
				
				
				
				break;
		}
	}
	
	function board(five)
	{
		pins = (conf.pins || [conf.pin]).map(function(p) {
			return five.Led(p);
		});
	}
	
	function init()
	{
		if(!(conf instanceof Object))
			conf = {};
		if(conf.list instanceof Array)
			list = conf.list;
	}
	
	init();
	
	this.set = modSet;
	this.board = board;
	return this;
}

module.exports = ledModule;
