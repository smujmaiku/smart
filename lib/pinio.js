var gpio = require('rpi-gpio');

function pinioModule(conf)
{
	var pinlist = {0:11, 1:12, 2:13, 3:15, 4:16, 5:18, 6:22, 7:7};
	var pintype = {};
	
	function debug(msg, level)
	{
		if(conf.debug >= level)
			console.log(msg);
	}

	function write(pin, state)
	{
		if(Object.keys(pinlist).indexOf(pin.toString()) < 0)
			return false;
			
		if(pintype[pin] != 'w')
		{
			setup(pin, 'w', state);
			return false;
		}
	
		var gpin = pinlist[pin];

		gpio.write(gpin, state, function(err)
		{
			debug('Write: ' + pin + '(' + gpin + '):' + state, 2);
			if(err != null)
			{
				debug('Error: Write(' + pin + ') ' + err, 1);
				pintype[pin] = '';
			}
		});
		return true;
	}
	
	function setup(pin, type, state)
	{
		if(Object.keys(pinlist).indexOf(pin.toString()) < 0)
			return false;
		pintype[pin] = '';
	
		var gpin = pinlist[pin];
	
		if(type == 'w')
			gpio.setup(gpin, gpio.DIR_OUT, function()
			{
				pintype[pin] = 'w';
				if(state != null)
					write(pin, state);
			});
	}
	
	this.write = write;
	this.setup = setup;
	
	return this;
}

module.exports = pinioModule;