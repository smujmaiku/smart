var fs = require('fs');

var Auth = require('./lib/auth.js');
var Socket = require('./lib/socket.js');

var config;
var socket;
var sockusers = {};

function debug(msg, level)
{
	if(config.debug >= level)
		console.log(msg);
}

function checkAuth(id)
{
	if(checkLogin(id))
	if(sockusers[id] instanceof Object)
	if(typeof sockusers[id].group == 'string')
		return true;
	return false;
}

function checkLogin(id)
{
	if(sockusers[id] instanceof Object)
	if(typeof sockusers[id].user == 'string')
		return true;
	return false;
}

function getHostId(host)
{
	for(var uid in sockusers)
	if(sockusers[uid].host == host)
		return uid;
	return null;
}

function setData(data)
{
	if(!(data instanceof Object))
		return;
	
	var tdata = {};
	
	for(var host in data)
	if(data[host] instanceof Object)
	{
		var uid = getHostId(host);
		if(typeof uid != 'string' || !(sockusers[uid].list instanceof Object))
			continue;
		
		tdata[host] = {};
		for(var key in data[host])
		if(sockusers[uid].list[key] instanceof Object)
		{
			sockusers[uid].list[key].val = data[host][key];
			tdata[host][key] = data[host][key];
		}
	}

	sendSet(true, tdata);
}

function sendAuth(id, sessid)
{
	if(!(sockusers[id] instanceof Object))
		return;
	
	socket.sockSend(id, 'auth', {
		auth: checkAuth(id),
		login: checkLogin(id),
		user: sockusers[id].user,
		group: sockusers[id].group,
		sess: sessid
	});
}

function sendSet(id, data)
{
	if(id === true)
		return Object.keys(sockusers).map(function(uid) {
			sendSet(uid, data);
		});
	
	if(checkAuth(id))
		socket.sockSend(id, 'set', data);
	else if(checkLogin(id) && typeof sockusers[id].host == 'string')
	{
		var tdata = {};
		if(sockusers[id] instanceof Object)
			tdata[sockusers[id].host] = data[sockusers[id].host];
		if(tdata[sockusers[id].host] instanceof Object)
			socket.sockSend(id, 'set', tdata);
	}
}

function sendHosts(users)
{
	if(users === true)
		users = Object.keys(sockusers);
	if(typeof users == 'string')
		users = [users];
	if(!(users instanceof Array))
		return;
	
	var data = {};
	
	for(var id in sockusers)
	if(sockusers[id] instanceof Object)
	if(typeof sockusers[id].host == 'string')
	if(sockusers[id].list instanceof Object)
		data[sockusers[id].host] = sockusers[id].list;
	
	users.map(function(id)
	{
		if(checkAuth(id))
			socket.sockSend(id, 'hosts', data);
		else if(checkLogin(id) && typeof sockusers[id].host == 'string')
		{
			var tdata = {};
			if(sockusers[id] instanceof Object)
				tdata[sockusers[id].host] = data[sockusers[id].host];
			if(tdata[sockusers[id].host] instanceof Object)
				socket.sockSend(id, 'hosts', tdata);
		}
	});
}

function sockHost(id, data)
{
	if(!(checkLogin(id)))
		return sendAuth(id);
	if(!(data instanceof Object))
		return;
	
	if(checkAuth(id) || sockusers[id].user == data.host)
		sockusers[id].host = data.host;
	else
		sockusers[id].host = null;
	
	sockusers[id].list = data.list;
	
	sendHosts(true);
}

function sockSet(id, data)
{
	if(!checkLogin(id))
		return sendAuth(id);
	if(!(data instanceof Object))
		return;
	
		//Remove unauthed sets
	if(!checkAuth(id))
	for(var k in data)
	if(k != sockusers[id].host)
		delete data[k];
	
	if(data instanceof Object && Object.keys(data).length > 0)
		setData(data);
}

function sockAuth(id, data)
{
	if(!(data instanceof Object))
		return;
	
		//Clear authed user
	sockusers[id].user = null;
	sockusers[id].group = null;
	sockusers[id].authed = null;
	
	function authhandler(user, group, sessid)
	{
			//Store user auth
		sockusers[id].user = user;
		sockusers[id].group = group;
		
			//Check if logged in
		if(typeof group == 'string')
			sockusers[id].authed = true;
		
		sendAuth(id, sessid);
		
		if(checkAuth(id))
			sendHosts(id);
	}
	
		//Check credentials and pass to authhandler
	auth.checkAuth(
		data.user,
		data.pass,
		authhandler
	);
}

function sockConnect(id, data)
{
	sockusers[id] = {};
	sendAuth(id);
}

function sockDisconnect(id, data)
{
	var host = sockusers[id].host;
	delete sockusers[id];
	
	if(typeof host == 'string')
		sendHosts(true);
}

function loadAuth(conf)
{
	debug('Main: Starting module auth', 1);
	
	conf.debug = config.debug;
	try
	{
			//Load extra confs
		var authconf = require(conf.conf);
		for(var key in authconf)
			conf[key] = authconf[key];
	} catch(e) {}
	
		//Load auth module
	auth = new Auth(
		conf,
		{}
	);
}

function loadSocket(conf)
{
	debug('Main: Starting module socket', 1);
	
	try
	{
			//Load extra confs
		var sockauth = require(conf.conf);
		for(var key in sockauth)
			conf[key] = sockauth[key];
	} catch(e) {}
	
		//Store options
	var opts = {
		debug: config.debug > 2,
		sslKey: conf.sslkey,
		sslCert: conf.sslcert,
		sslCa: conf.sslca
	};
	
	socket = new Socket(
		conf.port,
		'./html',
		null,
		{
			'connect': sockConnect,
			'disconnect': sockDisconnect,
			'auth': sockAuth,
			'host': sockHost,
			'set': sockSet
		},
		opts
	);
}

function main()
{
		//Load custom config
	if(typeof process.argv[2] == 'string')
		config = require(process.argv[2]);
		//Load default config
	if(!(config instanceof Object))
		config = require('./conf/server.json');
	
	loadAuth(config.auth);
	loadSocket(config.socket);
}

main();
