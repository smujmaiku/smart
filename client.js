var IoClient = require('socket.io-client');

var socket;

var config;
var modules = {};
var five, fiveio, board;

function sendHost()
{
	var list = JSON.parse(JSON.stringify(config.list));
	for(var k in list)
	{
		if(!(list[k] instanceof Object) || list[k].local)
		{
			delete list[k];
			continue;
		}
		
		delete list[k].conf;
	}
	
	socket.emit('host', {
		host: config.host,
		list: list
	});
}

function set(key, val, send)
{
		//Set value
	if(config.list[config.host] instanceof Object)
	if(config.list[config.host][key] instanceof Object)
		config.list[config.host][key].val = val;
	
	try
	{
		modules[key].set(val);
	} catch(e) {}
	
		//Send set
	if(send)
		sendSet(config.host, key, val);
}

function sendSet(host, key, val)
{
	var data = {};
	data[host] = {};
	data[host][key] = val;
	socket.emit('set', data);
}

function setData(data, send)
{
	if(!(data instanceof Object))
		return;
	
	if(data[config.host] instanceof Object)
	for(var key in data[config.host])
	if(config.list instanceof Object)
	if(config.list[key])
	{
		var val = data[config.host][key];
		set(key, val, send);
	}
}

function sendAuth()
{
	socket.emit('auth', {
		user: config.user,
		pass: config.pass
	});
}
	
function loadSocket()
{
	function onConnect()
	{
		sendAuth();
	}
	
	function onDisconnect()
	{
		if(config.list instanceof Object)
		Object.keys(config.list).map(function(key)
		{
			set(key, false);
		});
	}
	
	function onAuth(data)
	{
		if(data instanceof Object && data.login)
			return sendHost();
		setTimeout(sendAuth, 1000);
	}
	
	function onSet(data)
	{
		setData(data, false);
	}
	
	socket = new IoClient(config.url, {rejectUnauthorized: false});
	
	socket.on('connect', onConnect);
	socket.on('disconnect', onDisconnect);
	socket.on('auth', onAuth);
	socket.on('set', onSet);
}

function loadModules()
{
	function loadModule(key, conf)
	{
		var events = {
			set: function(val)
			{
				set(key, val, true);
			}
		};
		
		var mod;
		try
		{
			var Mod = require('./lib/mod_' + conf.mod + '.js');
			var modconf = JSON.parse(JSON.stringify(conf));
			modconf._key = key;
			mod = new Mod(modconf, events);
		} catch(e) {}
		
		return mod;
	}
	
	if(config.list instanceof Object)
	for(var key in config.list)
	if(config.list[key] instanceof Object && config.list[key].conf instanceof Object)
		modules[key] = loadModule(key, config.list[key].conf);
	
	if(typeof config.io == 'string')
	{
		five = require('johnny-five');
		fiveio = require(config.io);
                board = new five.Board({
                        io: new fiveio()
                });

                board.on('ready', function()
                {
			for(var key in modules)
			if(modules[key] instanceof Object && modules[key].board instanceof Function)
				modules[key].board(five);
		});
	}
}

function main()
{
		//Load custom config
	if(typeof process.argv[2] == 'string')
		config = require(process.argv[2]);
		//Load default config
	if(!(config instanceof Object))
		config = require('./conf/client.json');
	
	loadModules();
	loadSocket();
}
main();
